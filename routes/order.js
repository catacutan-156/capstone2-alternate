// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require("../controllers/order");
	const auth = require("../auth");

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] POST Routes
	// Create Order
	route.post("/add-to-cart", auth.verify, (req, res) => {
		let token = auth.decode(req.headers.authorization);
		let info = req.body;
		let isAdmin = token.isAdmin;
		let inputId = token.id;

		let data = {
			userId: inputId,
			userInput: info
		};

		if (isAdmin) {
			res.send("Please use a regular account to purchase products.");
		} else {
			controller.addToCart(data).then(outcome => res.send(outcome));
		};
	});

// [SECTION] GET Routes
	// Get Order

	// Get Orders

// [SECTION] UPDATE Routes
	// Update Order

// [SECTION] DELETE Routes
	// Delete Order

// [SECTION] Expose Route System
	module.exports = route;