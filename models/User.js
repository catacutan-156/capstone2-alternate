// [SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

// [SECTION] Schema
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "First name is required."]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required."]
		},
		middleName: {
			type: String,
			required: [true, "Middle name is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile number is required."]
		},
		gender: {
			type: String,
			required: [true, "Gender is required."]
		},
		homeAddress: {
			type: String,
			required: [true, "Home address is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false,
		}
	});

// [SECTION] Model
	module.exports = mongoose.model("User", userSchema);