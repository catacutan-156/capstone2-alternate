// [SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

// [SECTION] Schema
	const orderSchema = new mongoose.Schema({
		orders: [
			{
				userId: {
					type: String,
					required: [true, "User ID is required."]
				},
				productName: {
					type: String,
					required: [true, "Product name is required."]
				},
				quantity: {
					type: Number,
					required: [true, "You need to purchase at least 1."]
				},
				subTotal: {
					type: Number,
					required: [true, "Total amount for this product is required."]
				}
			}
		],
		totalOrderAmount: {
			type: Number,
			required: [true, "Total amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTION] Model
	module.exports = mongoose.model("Order", orderSchema);